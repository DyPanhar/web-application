<?php  
include("connection.php");
    $idpage = $_GET['id'];
    $query = "SELECT * FROM phone_contact ";
              
    $result = mysqli_query($conn,$query);
    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
    // var_dump($data[$idpage]);
    // var_dump($data);
    $contact = 0;
    foreach($data as $arr){
        if($arr['id'] == $idpage){
            $id = $arr['id'];
                $fname = $arr['firstname'];
                $lname = $arr['lastname'];
                $contact = $arr['phone_number'];
                $link = $arr['url_fb_social'];
                // echo $fname;
                // echo"<hr>";
        }
    }
?>

<?php
    if(isset($_POST['submit'])){
        $fnameUpdate = $_POST['fname'];
        $lnameUpdate =  $_POST['lname'];
        $contactUpdate = $_POST['contact'];
        $url_linkUpdate = $_POST['fb_link'];
        $sql = "UPDATE phone_contact
                SET firstname='$fnameUpdate',
                    lastname='$lnameUpdate',
                    phone_number='$contactUpdate',
                    url_fb_social='$url_linkUpdate'
                WHERE id = $id";
        if ($conn->query($sql) === TRUE) {
            header("location: crud.php");
          } else {
            echo "Error updating record: " . $conn->error;
          }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
       
       <div class="container">
            <form method="post" enctype="multipart/form-data">
             <div class="mb-3">
               <label class="form-label">FirstName :</label>
               <input  class="form-control" name="fname" type="text" placeholder="Enter your first name" <?php echo" value='$fname'"; ?> >
             </div>
             <div class="mb-3">
               <label class="form-label">LastName :</label>
               <input  class="form-control" name="lname" type="text" placeholder="Enter your last name" <?php echo" value='$lname'"; ?> >
             </div>

             <div class="mb-3">
               <label class="form-label">Phone Number :</label>
               <input  class="form-control" name="contact" type="text" placeholder="Enter your phone number" <?php echo" value='$contact'"; ?> >
             </div>
             
             <div class="mb-3">
               <label class="form-label">Facebook Link :</label>
               <input  class="form-control" name="fb_link" type="text" placeholder="Input the facebook link" <?php echo" value='$link'"; ?> >
             </div>

           <button type="submit" class="btn btn-success" name="submit">Submit</button>
     </form>
     </div>

    
</body>
</html>