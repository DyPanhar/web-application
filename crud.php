<?php  
include("connection.php");
session_start();

?>

<?php
if(isset($_POST['submit'])){
    $fname = $_POST['fname'];
    $lname =  $_POST['lname'];
    $contact = $_POST['contact'];
    $url_link = $_POST['fb_link'];
    $img = $_FILES;

    $tmp_name = $img['img']['tmp_name'];
    $file_name = $img['img']['name'];

    move_uploaded_file($tmp_name, 'upload_files/' . $file_name);

    $sql = 'INSERT INTO phone_contact(phone_number,firstname,lastname,url_fb_social,profile_picture)
            VALUES (?,?,?,?,?)';
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('sssss', $contact , $fname , $lname , $url_link ,$file_name);

    if($stmt->execute()){
        
        $_SESSION['success_message'] = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                                            User added successfully.
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>';
        header("Location: " . $_SERVER['PHP_SELF']);
        exit();
    }
    else{
        echo '  
            <div class="alert alert-success" role="alert">
                <p>Error adding user.</p>
            </div>';
    }
}

//Read function
$query = "SELECT * FROM phone_contact";
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_all($result, MYSQLI_ASSOC);

//Delete function
if (isset($_POST['delete'])) {
  $idToDelete = $_POST['id_to_delete'];
  $deleteQuery = "DELETE FROM phone_contact WHERE id = ?";
  $stmtDelete = $conn -> prepare($deleteQuery);
  $stmtDelete -> bind_param('i', $idToDelete);
  $stmtDelete -> execute();
}

$success_message = isset($_SESSION['success_message']) ? $_SESSION['success_message'] : '';
unset($_SESSION['success_message']);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container" style="width: 750px; margin-top: 50px;">
    <?php  echo  $success_message ?>
  
    <div>
                    <!-- Button trigger modal -->
    <div >
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
          Add user
        </button>
    </div>
                      <!-- Modal -->
       <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
             <div class="modal-content">
                 <div class="modal-header">
                   <h1 class="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
           <div class="modal-body">
              
           <form method="post" enctype="multipart/form-data">
             <div class="mb-3">
               <label class="form-label">FirstName :</label>
               <input  class="form-control" name="fname" type="text" placeholder="Enter your first name">
             </div>

             <div class="mb-3">
               <label class="form-label">LastName :</label>
               <input  class="form-control" name="lname" type="text" placeholder="Enter your last name">
             </div>

             <div class="mb-3">
               <label class="form-label">Phone Number :</label>
               <input  class="form-control" name="contact" type="text" placeholder="Enter your phone number">
             </div>
             
             <div class="mb-3">
               <label class="form-label">Facebook Link :</label>
               <input  class="form-control" name="fb_link" type="text" placeholder="Input the facebook link">
             </div>


             <div class="mb-3">
               <label class="form-label">Profile Picture :</label>
               <input  class="form-control" name="img" type="file">
             </div>

           <button type="submit" class="btn btn-success" name="submit">Submit</button>
     </form>


           </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
          
        </div>
          </div>
       </div>
      </div>
   </div>

        <div style="margin-top: 10px;" >
            <table class="table table-stripped border">
             <thead>
               <tr>
                <th>Id</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>Contact</th>
                <th>FB link</th>
                <th>Profile Picture</th>
                <th>Update</th>
                <th>Delete</th>
               </tr>
            </thead>
              
            <tbody>
              
            <?php  
              
              $query = "SELECT * FROM phone_contact ";
              
              $result = mysqli_query($conn,$query);
              $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

              foreach($data as $arr){
                $id = $arr['id'];
                $fname = $arr['firstname'];
                $lname = $arr['lastname'];
                $contact = $arr['phone_number'];
                $link = $arr['url_fb_social'];
                $img = $arr['profile_picture'];

                echo "<tr>";
                echo "<td>$id</td>
                      <td>$fname</td>
                      <td>$lname</td>
                      <td>$contact</td>
                      <td> <a href=$link>$link</a></td>
                      <td> <img src='upload_files/$img' alt='profile' height='40px;'></td>
                      <td> <a href='edit.php?id=$id'>edit</a></td>";
                      echo "<td>
                        <form method = 'post'>
                          <input type = 'hidden' name = 'id_to_delete' value = '$id'>
                          <button type = 'submit' class = 'btn btn-danger' name = 'delete'>Delete</button>
                        </form>
                    </td>";
                  echo "</tr>";
              }
            

            ?>
               <tr>
                
               </tr>

              
            </tbody>
            </table>
        </div>
       
</body>
</html>

  